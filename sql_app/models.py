from sqlalchemy import Boolean, ForeignKey, Float, Integer, String, Column, VARCHAR
from sqlalchemy.orm import relationship

from .database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(VARCHAR(255), index=True)
    hashed_password = Column(VARCHAR(255))
    is_active = Column(Boolean, default=True)

    items = relationship("Item", back_populates="users")


class Item(Base):

    __tablename__ = "items"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(VARCHAR(255), index=True)
    description = Column(VARCHAR(255), index=True)
    owner_id = Column(Integer, ForeignKey("users.id"))

    users = relationship("User", back_populates="items")
