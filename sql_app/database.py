from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

SQLALCHEMY_DATABASE_URl = "mysql+pymysql://qadb:qadb@123@127.0.0.1/new_data_3"

engine = create_engine(SQLALCHEMY_DATABASE_URl)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()  # declarative_base() that returns a class.
