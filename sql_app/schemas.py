from typing import List

from pydantic import BaseModel


class ItemBase(BaseModel):
    title: str
    description: str


class UserBase(BaseModel):
    email: str


class ItemCreate(ItemBase):
    pass


class UserCreate(UserBase):
    password: str


class Item(ItemBase):
    id: int
    owner_id: int

    class config:
        orm_mode = True  # erm_mode ittells the pydantic model to read the data evem if it is not in dict format, but its in orm_mode


class User(UserBase):
    id: int
    is_active: bool
    items: List[Item] = []

    class config:
        orm_mode = True
